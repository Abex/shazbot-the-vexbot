#define MAJOR 1
#define MINOR 2

#include "base.h" //allows for code completion outside of robotc
//nclude "joyMuxer.c" // actually in portDefs.c due to enum reliance. sorry.
#include "portDefs.c" // defines port usage and motor/sensor related helpers
#include "logger.c" // LCD control
#include "watchdog.c"
#include "lifterArm.c" // arm task
#include "drivetrain.c" // drivetraintask
#include "autonomous.c"

#pragma DebuggerWindows("Globals")
//#pragma DebuggerWindows("debugStream")
//#pragma DebuggerWindows("joystickSimple")
#pragma DebuggerWindows("VexLCD")

task main()
{
  stopAllMotors();
  running=Disabled;
  setSensorTypes(); // config sensors
  calcDebug(); // get mode
	clrLCD();

  startTask(loggerTask);
  startTask(watchDogTask);

  if(competitionMode) sleep(2000); // 2 sec sleep for MasterCPU startup
  // PRE AUTON
  //lifterArmCalibrate(); // calibrate lifter arm(move down to limit)
  stopAllMotors();
  while(running!=Shutdown)
  {
	  if(controlType==AutonomousJoy || (competitionMode && bIfiAutonomousMode && !bIfiRobotDisabled ))
	  {
	    running=AutonomousState;
	    motor[lifterLatch]=-127;
	    startTask(autonomousTask); // start autonomous
	    while(controlType==AutonomousJoy || (competitionMode && bIfiAutonomousMode && !bIfiRobotDisabled ))
	    	{EndTimeSlice();} // wait for end
	    running=Disabled; // stop
	    stopTask(autonomousTask);
	    stopAllMotors();
	  }
	  if(controlType!=AutonomousJoy && !bIfiRobotDisabled && !bIfiAutonomousMode) // remote
	  {
	    running=Remote; // start remote
	    motor[lifterLatch]=-127;
	    startTask(driveTrainTask);
	    startTask(lifterArmTask);
	    while(controlType!=AutonomousJoy && !bIfiRobotDisabled && !bIfiAutonomousMode)
	    {EndTimeSlice();}
	  	stopTask(driveTrainTask);
	  	stopTask(lifterArmTask);
	    stopAllMotors();
	  }
	  if(competitionMode && bIfiRobotDisabled)
	    {
	      stopTask(driveTrainTask);
	      stopTask(lifterArmTask);
	      running=Disabled;
	    }
    EndTimeSlice();
	}
}

#pragma systemFile
enum PCJoystickVal
{
  joy1_x1,
  joy1_y1,
  joy1_x2,
  joy1_y2,
  joy1_Buttons,
  joy1_TopHat,
};
struct MultiButton{
  TVexJoysticks SingleJoy;
  TVexJoysticks DoubleJoy;
  PCJoystickVal PCJoy; // http://www.robotc.net/wiki/NXT_Using_Joysticks#Buttons
  int           PCJoyOffset;
};
#define JOY(n,a,b,c,d) n.SingleJoy = a ; n.DoubleJoy = b ; n.PCJoy = c ; n.PCJoyOffset = d
TPCJoystick joystick;
int joy(MultiButton button)
{
  if(controlType==SingleJoy && button.SingleJoy!=-1)
    return vexRT[button.SingleJoy];
  else if(controlType==DualJoy && button.DoubleJoy!=-1)
    return vexRT[button.DoubleJoy];
  else if(controlType==PCJoy && button.PCJoy!=-1)
  {
    getPCJoystickSettings(joystick);
    switch(button.PCJoy)
    {
    case joy1_x1:
      return joystick.joy1_x1;
    case joy1_x2:
      return joystick.joy1_x2;
    case joy1_y1:
      return joystick.joy1_y1;
    case joy1_y2:
      return joystick.joy1_x2;
    case joy1_Buttons:
      return ((joystick.joy1_Buttons & (1 << (button.PCJoyOffset - 1))) != 0); // stolen from the libs
    case joy1_TopHat:
      return joystick.joy1_TopHat==button.PCJoyOffset;
    }
  }
  return 0;
}

task driveTrainTask()
{
  float speedMultiplier=1;
  while(running==Remote)
  {
    speedMultiplier=1;
    if(joy(driveSlowdown)) // speed
      speedMultiplier=2;
    if(joy(driveSlowdown2))
      speedMultiplier=4;

    int rx = mjoy(driveX,3)/speedMultiplier; //temp vars for performance
    int ry = mjoy(driveStrafe,3)/speedMultiplier;
    int lx = mjoy(driveRotate,3)/speedMultiplier;

    if(controlType==DualJoy)
    {
      rx += mjoy(driveSecondaryX,3)/3;
      ry += mjoy(driveSecondaryStrafe,3)/3;
      lx += joy(driveSecondaryRotateA)*40 - joy(driveSecondaryRotateB)*40;
    }

    m(-(rx+lx+ry),driveFrontLeft); //calc & set
    m((rx-lx-ry),driveFrontRight);
    m((rx+lx-ry),driveRearLeft);
    m(-(rx-lx+ry),driveRearRight);

    if(joy(intakeControl)) SensorValue[intakeDig]=1;
    if(joy(intakeControl2)) SensorValue[intakeDig]=0;

    EndTimeSlice(); // allow for other threads
  }
}

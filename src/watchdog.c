runState oldRun;
task watchDogTask()
{
  while(running!=Shutdown)
  {
    if(false && competitionMode==true && !bVEXNETActive && nVexRCReceiveState == vrNoXmiters) // halt if no joystick (& a joystick is expected)
    {
      if(running!=NoConnection)
      {
        oldRun=running;
        running=NoConnection;
      }
      stopAllMotors();
    }
    else if(running==NoConnection)
      running = oldRun;

    if(!SensorValue[EStop]) // Emergency Stop
    {
      bLCDBacklight = false;
      running=Shutdown;
    }
    EndTimeSlice();
    if(running==NoConnection) // FIRST FRC RSL code ex battery
      SensorValue[statusLED] = !nPgmTime%400<200;
    else if(running==AutonomousState)
      SensorValue[statusLED] = !1;
    else if(running==Remote)
      SensorValue[statusLED] = !nPgmTime%1500<1400;
    else if(running==Disabled)
      SensorValue[statusLED] = !nPgmTime%1800<900;
    else
      SensorValue[statusLED] = !0;
  }
}

struct tuple {
  int a;
  int b;
};
tuple lifterUlimit;
tuple lifterLlimit;
int lifterULDelta=2300;
int offCheck=50;
tuple lifterPrefVal;
void lifter0(int val)
{
  if(val<0 && SensorValue[lifter0Pot]<(lifterLlimit.a+200))
    val=val/3;
  if((val<0 && !SensorValue[lifter0Stop]) || (val>0 && lifterUlimit.a<=SensorValue[lifter0Pot]))
    val=0;
  motor[lifter0_0]=-val;
  motor[lifter0_1]=val;
}
void lifter1(int val)
{
  if(val<0 && SensorValue[lifter1Pot]<(lifterLlimit.b+200))
    val=val/3;
  if((val<0 && !SensorValue[lifter1Stop]) || (val>0 && lifterUlimit.b<=SensorValue[lifter1Pot]))
    val=0;
  motor[lifter1_0]=val;
  motor[lifter1_1]=-val;
}
task lifterArmTask()
{
  lifterPrefVal.a=0;
  lifterPrefVal.b=0;
  lifterLlimit.a=0;
  lifterLlimit.b=0;
  lifterUlimit.a=4096;
  lifterUlimit.b=4096;
  while(running==Remote)
  {
    int analogeJoyVal = mjoy(lifterAnalog,10);
    if(analogeJoyVal)
    {
      while(analogeJoyVal)
      {
        analogeJoyVal = mjoy(lifterAnalog,10);
        lifter0(analogeJoyVal);
        lifter1(analogeJoyVal);
      }
      lifterPrefVal.a = SensorValue[lifter0Pot];
      lifterPrefVal.b = SensorValue[lifter1Pot];
    }
    else
    {
      if(SensorValue[lifter0Stop] && (lifterPrefVal.a<SensorValue[lifter0Pot]-offCheck || lifterPrefVal.a>SensorValue[lifter0Pot]+offCheck)) //+-20
      {//lifter 0 is wrong
        lifter0((lifterPrefVal.a-SensorValue[lifter0Pot])/20);
      }
      else
        lifter0(0);

      if(SensorValue[lifter1Stop] && (lifterPrefVal.b<SensorValue[lifter1Pot]-offCheck || lifterPrefVal.b>SensorValue[lifter1Pot]+offCheck)) //+-20
      {//lifter 1 is wrong
        lifter1((lifterPrefVal.b-SensorValue[lifter1Pot])/20);
      }
      else
        lifter1(0);

      if(!SensorValue[lifter0Stop] && !SensorValue[lifter1Stop])
      {
        lifter0(0);
        lifter1(0);
        lifterLlimit.a = SensorValue[lifter0Pot];
        lifterLlimit.b = SensorValue[lifter1Pot];
        lifterUlimit.a = lifterLlimit.a+lifterULDelta;
        lifterUlimit.b = lifterLlimit.b+lifterULDelta;
      }
    }
    EndTimeSlice();//allow for other tasks
  }
}

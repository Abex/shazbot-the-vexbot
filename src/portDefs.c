#pragma systemFile
typedef enum {
  Disabled,
  AutonomousState,
  Remote,
  NoConnection,
  Stopped,
  Shutdown,
} runState;

typedef enum {
  SingleJoy,
  DualJoy,
  PCJoy,
  AutonomousJoy,
} EcontrolType;

runState running;
bool debug = true;
bool competitionMode = false;
EcontrolType controlType = DualJoy;

#include "joyMuxer.c" // I AS SOOO SORRY BUT ITS SOO MUCH NICER HERE

const tMotor driveFrontLeft  = port2; // drive motors
const tMotor driveFrontRight = port3;
const tMotor driveRearLeft   = port4;
const tMotor driveRearRight  = port5;

const tMotor lifter0_0       = port1; // lifter motors
const tMotor lifter0_1       = port10;
const tMotor lifter1_0       = port6;
const tMotor lifter1_1       = port7;

const tMotor lifterLatch     = port9;

const tSensors intakeDig     = dgtl5;

const tSensors lifter0Pot    = in3;  // lifter sensors
const tSensors lifter1Pot    = in4;
const tSensors lifter0Stop   = dgtl3;
const tSensors lifter1Stop   = dgtl4;

const tSensors powerExpander = in2; // power expander status
const tSensors debugPot      = in1; // debug selector
const tSensors statusLED     = dgtl2; // status LED
const tSensors EStop         = dgtl1;

MultiButton driveX;
MultiButton driveStrafe;
MultiButton driveRotate;

MultiButton lifterAnalog;

MultiButton driveSlowdown;
MultiButton driveSlowdown2;

MultiButton intakeControl;
MultiButton intakeControl2;

MultiButton driveSecondaryX;
MultiButton driveSecondaryStrafe;
MultiButton driveSecondaryRotateA;
MultiButton driveSecondaryRotateB;

void setSensorTypes()
{
	SensorType[debugPot]         = sensorPotentiometer;
	SensorType[powerExpander]    = sensorAnalog;
	SensorType[statusLED]        = sensorDigitalOut;
	SensorType[EStop]            = sensorDigitalIn;

  SensorType[intakeDig]        = sensorDigitalOut;

	SensorType[lifter0Stop]      = sensorDigitalIn;
	SensorType[lifter1Stop]      = sensorDigitalIn;
	SensorType[lifter0Pot]       = sensorPotentiometer;
	SensorType[lifter1Pot]       = sensorPotentiometer;

	JOY(driveX,Ch2,Ch2,joy1_x2,0);
	JOY(driveStrafe,Ch1,Ch1,joy1_y2,0);
	JOY(driveRotate,Ch4,Ch4,joy1_y1,0);
	JOY(driveSlowdown,Btn5U,Btn5U,joy1_Buttons,5);
	JOY(driveSlowdown2,Btn5D,Btn5D,joy1_Buttons,7);

	JOY(lifterAnalog,Ch3,Ch3Xmtr2,joy1_x1,0);

	JOY(intakeControl,Btn6D,Btn6DXmtr2,joy1_Buttons,8);
	JOY(intakeControl2,Btn6U,Btn6UXmtr2,joy1_Buttons,6);

	JOY(driveSecondaryX,-1,Ch2Xmtr2,-1,0);
	JOY(driveSecondaryStrafe,-1,Ch1Xmtr2,-1,0);
	JOY(driveSecondaryRotateA,-1,Btn5UXmtr2,-1,0);
	JOY(driveSecondaryRotateB,-1,Btn5DXmtr2,-1,0);
}

void calcDebug()
{
  /*
  0   - 45deg   Competiton
  45  - 90deg   Debug 2Joy
  90  - 135deg  Debug 1Joy
  135 - 180deg  Debug PCJoy
  180 - 225deg  Debug Autonomous
  225 - 270deg  Off
  */
  int dbPotVal = SensorValue[debugPot]/683;
  //writeDebugStream("%d",dbPotVal);
  switch (dbPotVal)
  {
    case 0:
      debug=false;
      controlType=DualJoy;
      competitionMode=true;
      break;
    case 1:
      controlType=DualJoy;
      break;
    case 2:
      controlType=SingleJoy;
      break;
    case 3:
      controlType=PCJoy;
      break;
    case 4:
      controlType=AutonomousJoy;
      break;
    default:
      stopAllTasks();
      break;
  }
}

int cleanVal(int val,int round); //headers for fns (below)
int motorClean(int val);

int cleanVal(int val,int round) // remove issues with close to 0 values
{
  return (abs(val)<round)?0:val;
}
int motorClean(int val)
{
  return cleanVal(val,14);
}
void m(int val, tMotor a) // set motor & clean 1x
{
  motor[a] = motorClean(val);
}
void m(int val, tMotor a, tMotor b )// set motor & clean 2x
{
  val = motorClean(val);
  motor[a] = val;
  motor[b] = val;
}
void m(int val, tMotor a, tMotor b ,tMotor c, tMotor d)// set motor & clean 4x
{
  val = motorClean(val);
  motor[a] = val;
  motor[b] = val;
  motor[c] = val;
  motor[d] = val;
}

void stopAllMotors()
{
	motor[port1] = 0;
	motor[port2] = 0;
	motor[port3] = 0;
	motor[port4] = 0;
	motor[port5] = 0;
	motor[port6] = 0;
	motor[port7] = 0;
	motor[port8] = 0;
	motor[port9] = 0;
	motor[port10]= 0;
}

int mjoy(MultiButton jc, int round) // cleaned joystick (adjustable)
{
  return cleanVal(joy(jc),round);
}
int mjoy(MultiButton jc) // cleaned joystick
{
  return cleanVal(joy(jc),5);
}

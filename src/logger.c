typedef enum {
  None,
  Battery,
  Version,
  Debug,
  IBattery,
  Laser,
} lcdScreen;
lcdScreen currentScreen = Laser;
/*
LEFT = 1
CENTER = 2
left&center = 3
RIGHT = 4
left&right = 5
center&right = 6
ALL = 7
*/
void clrLCD()
{
  clearLCDLine(0);
  clearLCDLine(1);
}
task loggerSleeperTask()
{
  bLCDBacklight = true;
  char tmpStr[18];
  switch (currentScreen)
  {
    case None:
      break;
    case Laser:
      clrLCD();
      tmpStr[0]=0xFB;
      displayLCDString(0,1,tmpStr);
      displayLCDCenteredString(1,"Gentoo Laser");
      sleep(100);
      for(int i=1;i<15;i++)
      {
        tmpStr[i]=0xFB;
        tmpStr[i-1]=0x2D;
        displayLCDString(0,1,tmpStr);
        sleep(100);
      }
    case Debug:
      displayLCDString(0,6,"H");
      clrLCD();
      displayLCDString(1,6,"H");
      clrLCD();
      if(debug)
        displayLCDString(1,0,"DEBUG");
      else
        displayLCDString(1,0,"CMPTN");

      switch (controlType)
      {
        case DualJoy:
        	displayLCDString(1,6,"DUAL");
        	break;
        case SingleJoy:
        	displayLCDString(1,6,"SINGLE");
        	break;
        case PCJoy:
        	displayLCDString(1,6,"PCJOY");
        	break;
        case AutonomousJoy:
        	displayLCDString(1,6,"AUTO");
        	break;
      }
      sprintf(tmpStr,"L VATEINN v%02hhu.%02hhu",(unsigned char)MAJOR,(unsigned char)MINOR);
      tmpStr[1]=0x92; //debug joycontrol mode
      displayLCDCenteredString(0,tmpStr);
      sleep(2000);
      clrLCD();
      displayLCDString(0,0,"BUILT: ");
      displayLCDString(0,11,__TIME__);
      displayLCDString(1,5,__DATE__); //bVEXNETActive
      sleep(2000);
      currentScreen=None;
    case Battery:
      do
      {
        clrLCD();
        displayLCDString(0,0,"MAIN  DRVE BKUP");
        sprintf(tmpStr,"%04.2f  %04.2f  %04.2f",((float)nAvgBatteryLevel)/1000,((float)SensorValue[powerExpander])/280 ,((float)BackupBatteryLevel)/1000);
        displayLCDString(1,0,tmpStr);
        sleep(5000);
      }
      while(currentScreen==Battery);
      clrLCD();
      break;
    case IBattery:
      while(currentScreen==IBattery)
      {
        displayLCDString(0,0,"MAIN  DRVE BKUP");
        sprintf(tmpStr,"%04.2f  %04.2f  %04.2f",((float)nImmediateBatteryLevel)/1000,((float)SensorValue[powerExpander])/280,((float)BackupBatteryLevel)/1000);
        displayLCDString(1,0,tmpStr);
        sleep(500);
      }
      break;
  }
  bLCDBacklight = false;
}
task loggerTask()
{
  startTask(loggerSleeperTask);// Startup debug
  int lcdButton=0;
  while(running!=Shutdown)
  {
    lcdButton=0;
    while(running!=Shutdown)
    {
      if(lcdButton!=nLCDButtons)
      {
        if(nLCDButtons==0)
          break;
        lcdButton = nLCDButtons;
      }
      EndTimeSlice();
    }
    writeDebugStream("%d",lcdButton);
    switch(lcdButton)
    {
    case 1:
      currentScreen=Version;
      break;
    case 2:
      if(currentScreen==Battery)
        currentScreen=None;
      else
        currentScreen=Battery;
      break;
    case 4:
      currentScreen=Debug;
      break;
    case 6:
      if(currentScreen==IBattery)
        currentScreen=None;
      else
        currentScreen=IBattery;
      break;
    case 7:
      currentScreen=Laser;
    }
    stopTask(loggerSleeperTask);
    clrLCD();
    if(currentScreen!=None)
      startTask(loggerSleeperTask);
    else
      bLCDBacklight=false;
    EndTimeSlice();
  }
}

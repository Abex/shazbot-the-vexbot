/*
Quad encoder on Digital 1 &2
Motor attached to Quad on Motor 1
Writes Array of Objects (Tuple deg/.5sec, batt mvolt) in JSON format to debug stream
*/
task main()
{
  SensorType[dgtl1] = sensorQuadEncoder;
  SensorType[dgtl2] = sensorQuadEncoderSecondPort;
  motor[port1]=127;
  int l = 0;
  int v = 0;
  writeDebugStream("[");
  while(1)
  {
    v = l-SensorValue[dgtl1];
    if(v<0) v+=360;
    if(v>360) v-=360;
    writeDebugStream("{%d,%d},",n,nImmediateBatteryLevel);
    l=SensorValue[dgtl1]
    sleep(500);
  }
}